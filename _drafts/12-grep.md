### `grep` --- Grep is used to parse a file for certain content

The `grep` command is useful for pulling specific content from a file. You can run
`grep` to examine the contents without opening the file in vi or similar file viewer.
This can be very useful, particulary when searching for a word or phrase in multiple files.

Please follow along:
```bash
$ grep 206\.170 1998.dat
```
```
1998/03/891219236.5426.txt|206.170.68.60|US|United States
1998/05/895160496.11005.txt|206.170.31.182|US|United States
1998/05/895252660.20888.txt|206.170.31.182|US|United States
1998/05/896579625.31405.txt|206.170.185.101|US|United States
```
{: output}

In this example we used grep to look for a specific IP address range prefix in the 
file 1998.dat. Please note the `\` before the `.` in the grep command. This is used to
escape the `.`. Otherwise the `.` is treated as a wildcard and will match any character.

We can also use grep to search for a string of characters in many files
using the command:

```bash
$ grep 206.170 *
```

We can specify that we only want to look for matches at the beginning or end of the file using the`^` or `$` respectively.

The below command looks for the string at the beginning of each line:
```bash
$ grep ^1998/03/891219 1998.dat
```
```
1998/03/891219128.5403.txt|206.175.103.56|US|United States
1998/03/891219139.5404.txt|205.184.187.47|US|United States
1998/03/891219144.5405.txt|205.139.129.162|US|United States
1998/03/891219148.5406.txt|208.17.113.108|US|United States
1998/03/891219152.5407.txt|207.159.82.7|US|United States
1998/03/891219156.5408.txt|207.105.189.121|US|United States
1998/03/891219203.5423.txt||Malformed IP address|
1998/03/891219210.5424.txt|207.115.33.229|US|United States
1998/03/891219215.5425.txt|209.152.84.95|US|United States
1998/03/891219236.5426.txt|206.170.68.60|US|United States
```
{: output}

```bash
$ grep na$ 1998.dat
```

This command will look for the string `na` at the end of each line.

A very usefull function of grep, is the ability to use it in conjuction with pipe(|).
We can pipe the output of one command into grep. In this way we can pull out only the information
we are interested in.

Here is an example:
```bash
$ head -n 50 1998.dat | grep 206.212
```
```
1998/03/891020025.3222.txt|206.212.231.88|US|United States
1998/04/891608754.26624.txt|206.212.231.88|US|United States
1998/04/891661453.27625.txt|206.212.231.88|US|United States
1998/04/891661486.27627.txt|206.212.231.88|US|United States
1998/04/891665321.3293.txt|206.212.231.88|US|United States
```
{: output}

This command does two things. First it outpus the first 50 lines of the file 1998.dat. Finally it pipes this output through grep and filters for lines containing `206.212`.

 
 

