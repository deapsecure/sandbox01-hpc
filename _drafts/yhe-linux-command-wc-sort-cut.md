### `wc` --- Word Count

`wc` command: it counts the number of lines, words, and characters in files 
(from left to right, in that order).
Let's try this now:
```bash
$ wc 1998.dat
```
```
1097  2052 62003 1998.dat
```
{: .output}
If we run `wc -l` instead of just `wc`,
the output shows only the number of lines per file:
```bash
$ wc -l 1998.dat
```
{: .output}
> ## Activity: Try other flags
>
> We already took a peek at the `-l` flag.
> Now let's peek at some other flags on `wc` command:
>
> * `wc -c`
> * `wc -m`
> * `wc -w`
> * `wc -L`
> * 
> * Could you explain what the flags mean? What are they short for?
{: .callout}

### `sort` --- Sort Content

`sort` command: it is used to sort a file, arranging the records in a particular order. By default, the sort command sorts file assuming the contents are ASCII. Using options in sort command, it can also be used to sort numerically.
Let's try this now:(We only take a look at the last four line)
```bash
$ sort ip-1998.txt
```
```
38.30.22.93
38.9.32.2
4.12.29.235
4.4.18.88
```
{: .output}
Let's try to use sort -n:
```bash
$ sort -n ip-1998.txt
```
```
210.61.114.1
210.69.7.197
216.0.22.11
226.232.201.8
```
{: .output}
The `-n` option specifies a numerical rather than an alphanumerical sort.

### `uniq` --- Unique Line Matching

`uniq` command: it is used to report or omit repeated lines.
Let's try this now:
```bash
$ uniq countries-1998.txt
```
```bash
$ sort countries-1998.txt | uniq
```
The `uniq` command has a `-c` option which gives a count of the 
number of times a line occurs in its input.
```bash
$ sort countries-1998.txt | uniq -c
```
{: .output}


### `cut` --- Cutting out the sections from each line

`cut` command: is used to remove or 'cut out' certain sections of each line in the file,
and `cut` expects the lines to be separated into columns by a <kbd>Tab</kbd> character.
A character used in this way is a called a **delimiter**.
You can use the `-d` option to specify the `|` as our delimiter character.
We have also used the `-f` option to specify that we want to extract the second field (column).
Let's try this now:
```bash
$ cut -d "|" -f 2 2000.dat
```
```
63.17.146.248
63.38.73.109
63.38.73.109
200.28.31.2
```bash
$ cut -d "|" -f 3 2000.dat
```
```
US
US
US
CL
```
{: .output}

How could you extend this pipeline (using other commands) to find
out what countries the file contains (without any duplicates in their names)?

## Solution
```bash
$ cut -d "|" -f 2 2000.dat | sort | uniq
```
{: .solution}

### `>` --- Redirecting Output
### `>>` --- Appending Redirected Output

`>` operator: is used to redirect your output. By default, stdout and stderr are printed to your terminal-that's why you can see them at all. 
But we can redirect that output to a file using the `>`
Let's try this now:
```bash
$ echo hello
```
```
hello
```
```bash
$ echo hello > hello.txt
$ cat hello.txt
```
```
hello
```
The second `echo` didn’t print anything to the terminal because we redirected its output to a file named hello.txt. It does two things:
* It creates a file named hello.txt if it doesn’t exist;
* It replaces hello.txt’s contents with the new contents
So if hello.txt already existed, and we did `echo hello > hello.txt`, it would now have only hello in it. If you want to append to the file, rather than replacing its contents, you can use the `>>` operator:
```bash
$ echo hello again >> hello.txt
$ cat hello.txt
```
```
hello
hello again
```
{: .output}

### `|` --- Pipes

`|` operator: is used to connect the standard output of one command to the standard input of another.
Let's try this now:
```bash
$ less 2000.dat | head -n 3
```
```
2000/01/946769065.16975.txt|63.198.30.107|US|United States
2000/01/946769072.16976.txt|172.16.1.2|-|-
2000/01/946769090.16977.txt|209.254.0.20|US|United States
```
Above, we’ve connected less to head. Pipes are great for taking output of one command and transforming it using other commands. They’re a key part of the Unix philosophy of “small sharp tools”: since commands can be chained together with pipes, each command only needs to do one thing and then hand it off to another command.
{: .output}
