# DeapSECURE SANDBOX Lesson - Intro to HPC

This is a **sandbox** repository to help lesson contributors play with
a lesson repo, including performing `git push` operation.

> ## CAUTION
> This is not a real lesson.
> Do not do editing with the expectation that it will be published as part of the underlying lesson!
> Since the sandbox repo maybe shared with others,
> you must be ready for changes to be overwritten by others.



## Contributing

We welcome all contributions to improve the lesson! Maintainers will do their best to help you if you have any
questions, concerns, or experience any difficulties along the way.

We'd like to ask you to familiarize yourself with our [Contribution Guide](CONTRIBUTING.md) and have a look at
the [more detailed guidelines][lesson-example] on proper formatting, ways to render the lesson locally, and even
how to write new episodes.

## Maintainer(s)

* The DeapSECURE team.
  Please use https://gitlab.com/deapsecure/sandbox01-hpc/-/issues
  to discuss issues or ideas with the team.

## Authors

A list of contributors to the lesson can be found in [AUTHORS](AUTHORS)

## Citation

To cite this lesson, please consult with [CITATION](CITATION)

[lesson-example]: https://carpentries.github.io/lesson-example
