---
title: Setup
---

## SSH Client

The most important software to connect to HPC systems
is the Secure Shell (SSH) client.

### Windows

If you are already on Windows 10, you may already have `ssh`
included in your OS.
Windows users now have multiple options to choose from.
Putty is the easiest software for new users to get started.
More advanced users are recommended to use MobaXTerm.


#### Native SSH client (Windows 10 and later)

To access the `ssh` client in your own machine,
please launch the command prompt
(Press <kbd>Win</kbd>+<kbd>R</kbd>, type `cmd`,
then <kbd>Enter</kbd>).
Once the command prompt appear, type `ssh` and
press <kbd>Enter</kbd>.
If a message that starts with something like:
`usage: ssh [-1246AaCfgKkMNnqsTtVvXxYy] [-b bind_address] ...`
appears, then you can use this SSH client and not install anything!


#### PuTTY SSH client

For Windows, the easiest client to use is **PuTTY**,
which can be downloaded from the following link:
[https://www.chiark.greenend.org.uk/~sgtatham/putty/](
    https://www.chiark.greenend.org.uk/~sgtatham/putty/
).
There is an installer version and single executable version of the program.
Installer version is recommended to get the complete suitable
available at your fingertips.
If you do not have an admin right to install programs on the computer, then
please download the individual "putty.exe" file.
(For file transfer, you will also want "pscp.exe".)
Save the executable file(s) on a location that's easy to access,
such as your Desktop.

**PuTTY manual page**:
[https://www.chiark.greenend.org.uk/~sgtatham/putty/docs.html](
    https://www.chiark.greenend.org.uk/~sgtatham/putty/docs.html
).
Useful reference to know all the features of the program, and contains
guidance to troubleshooting.


#### MobaXTerm

There is a more powerful remote access program that you can also use on Windows,
named **MobaXterm**.
The Home edition is sufficiently good for most people:
[https://mobaxterm.mobatek.net/download-home-edition.html](
    https://mobaxterm.mobatek.net/download-home-edition.html
).
MobaXTerm is more than just an SSH client!
It also include X11 display server (to display remote applications via X11 protocol),
remote desktop client, and even a local UNIX-like environment via Cygwin.


When installing newer versions MobaXTerm (around version 12 or so),
please also download the `CygUtils.plugin` from
[the Plugins download page](https://mobaxterm.mobatek.net/plugins.html)
so that you can have local UNIX shell environment.
Later you may find it helpful to have the ability to operate
in a UNIX-like environment even on Windows.


### Mac OS X and Linux

Mac OS X and Linux computers already have `openssh` installed.
To test, open your terminal and type `ssh`.
The command should return a help message like this:

    usage: ssh [-1246AaCfgKkMNnqsTtVvXxYy] [-b bind_address] [-c cipher_spec]
	       [-D [bind_address:]port] [-e escape_char] [-F configfile]
	       [-I pkcs11] [-i identity_file]
	       [-L [bind_address:]port:host:hostport]
	       [-l login_name] [-m mac_spec] [-O ctl_cmd] [-o option] [-p port]
	       [-R [bind_address:]port:host:hostport] [-S ctl_path]
	       [-W host:port] [-w local_tun[:remote_tun]]
	       [user@]hostname [command]



## Hands-On Files (Optional)

Your instructor may ask you to download the hands-on packages from
the following web address:

>   [{{ page.root }}{%link files/module-hpc.zip %}]({{ page.root }}{%link files/module-hpc.zip %})

(Independent learners also need to download this file in order to
perform the hands-on exercises.)

We recommend that you create a folder called `CItraining` in your home directory,
After downloading, unzip the file above to the `CItraining` folder.
This process will create a subfolder called `module-hpc` inside `CItraining`.
If all is well, inside `module-hpc` you should see folders like
`results`, `Spam_bash`, and `Unix`, and possibly others.

{% include links.md %}
