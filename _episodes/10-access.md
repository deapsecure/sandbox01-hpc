---
title: "Accessing HPC"
teaching: 5
exercises: 5
questions:
- "How do we access a modern HPC system?"
- "How do we interact with a basic HPC interface?"
objectives:
- "Connecting to an HPC system using secure shell"
keypoints:
- "An HPC cluster is remotely accessed using secure shell"

---

## About Turing HPC Cluster

Turing is a high-performance computer, or supercomputer,
that is physically located in the ITS data center,
at the Old Dominion University's Norfolk campus.
Turing is a cluster of servers ("nodes") that are connected using
high-performance networks.
(Because of this, the term "cluster" is often used interchangeably
with "HPC".)
There are over 250 servers available for performing user computation
(these servers are called "compute nodes").
Each server has up to 32 CPU cores[^1] and at least 128 GB of RAM.
In total, Turing has over 6000 CPU cores and
29 TB (terabytes) of aggregated RAM.

[^1]: Older compute nodes have 16 or 20 cores. All newer ones have 32 CPU cores per node.

> ## Think about it!
>
> How many CPU cores and how much RAM does your laptop have?
> How does it compare to a compute node on Turing?
>
> If you have a computation that takes 24 hours to complete on your
> laptop (assuming it takes advantage of all the CPU cores),
> what is the shortest amount of time it could *potentially* take
> to complete on a single compute node on Turing that has 32 CPU cores?
> Assume that the CPU speed (clock frequency) is the same in both cases.
{: .challenge}

Turing cluster uses
[Red Hat Enterprise Linux](
    https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux
)
as its operating system (OS).
Linux is a
[free UNIX-like operating system](https://www.linux.com/what-is-linux/).

In addition to the compute nodes, there are additional servers that
perform other tasks, such as serving the files that are used by the users,
and "login node".
(We will cover storage and data transfer in a latter part of this training.)
Login node is the computer that interacts directly with the users when
they first connect to an HPC cluster.


## Accessing an HPC System

HPC systems are remotely accessed via Internet and shared among many users.
Typically, a **secure shell** (ssh) client is used to access these systems.
Interaction and data transfer between user's computer and Turing takes place
through a secure, encrypted channel.

Turing cluster has the following Internet address:

    turing.hpc.odu.edu

Use your ODU MIDAS username and password to connect to Turing's login node.
In the following section and throughout the workshop, please substitute
the string `YOUR_MIDAS_ID` with *your own* MIDAS ID.

### Access from Mac OS X and Linux Computers

For Mac, Linux, and other UNIX(-like) systems, open your terminal
program and type

```bash
$ ssh -l YOUR_MIDAS_ID turing.hpc.odu.edu
```
or

```bash
$ ssh YOUR_MIDAS_ID@turing.hpc.odu.edu
```

Then follow prompt to enter password.
(There is no character printed on the screen when you type your password.
This is normal; just continue.)


### Acccess from Windows Computers

For Windows users, download or install **PuTTY** program, as described
in the [Setup](../setup.html) section.
Open PuTTY client, a dialog will appear:

![Image of PuTTY connection dialog](../assets/img/gui-putty-dialog.png)

Enter `turing.hpc.odu.edu` in the "Host Name" field,
then click the "Open" button.
Follow prompt for user name and password.
The first time PuTTY connect to a remote computer, it will issue a
warning like this:

> **PuTTY Security Alert**
>
> The server's host key is not cached in the registry.
> You have no guarantee that the server is the computer you think
> it is.<br/>
> The server's rsa2 key fingerprint is: ...<br/>
> If you trust this host, hit Yes to add the key to PuTTY's cache
> and carry on connecting.<br/>
> If you you want to carry on connecting just once, without adding
> the key to the cache, hit No.<br/>
> If you do not trust this host, hit Cancel to abandon the connection.
>


This warning is normal (it is part of the security of SSH protocol).
Just click the "Yes" button and connect.


> ## Exercise: Connecting to Turing
> Now, it's time for action!
> Please connect to Turing using the appropriate SSH client for your machine.
> Make sure that you are to establish connection.
{: .challenge}

Once logged in, you will be greeted with a prompt that looks like this:

```bash
[YOUR_MIDAS_ID@turing1 ~]$
```

This is the prompt of the UNIX shell, which, in this case is **tcsh**.
We will cover the basic interaction with UNIX shell in the next episode.

A few items printed on the shell prompt are worth explaining here:

* **turing1**: This is `turing1`, which is Turing cluster's login node.
In the UNIX world, the name of the computer is printed on the prompt:
it is very common for a UNIX user to make connections to many
remote computers from a single computer, therefore this bit of information
is extremely important!

* **YOUR_MIDAS_ID**: Your user ID is printed on the prompt.

* The `~` (tilde) character in the prompt above denotes your home
directory.
This is the *current working directory* of the shell.
We will learn more about this in
[the next episode]({{ page.root }}{% link _episodes/11-shell.md %}).


### Disconnecting

To exit and disconnect from Turing (the remote machine),
simply type `exit` and press <kbd>Enter</kbd>.


> ## Do You Know?
> Despite the lack of awareness among typical computer users,
> Linux is a very popular operating system in the HPC world.
> In fact, by June 2018,
> [*all* TOP500 supercomputers are using Linux](
>     https://www.top500.org/statistics/details/osfam/1
> ) as its operating system.
>
> The "SSH + UNIX shell" access method described in this lesson is not
> unique to HPC systems.
> In fact, with the right software (SSH server), any network-connected
> computers (whether using Linux, Mac OS, and even Windows) can be
> remotely accessed using SSH.
>
> The majority of servers all around the world that provide us services
> (think of Google, Facebook, Amazon, ...) run Linux or UNIX of some
> sort under the hood.
> Many services in the cloud today also run Linux.
> Linux/UNIX skills are highly marketable in today's heavily digitized world.
{: .callout}


{% include links.md %}

